import React, { Component, lazy, Suspense } from 'react';
import { Line, Bar } from 'react-chartjs-2';
import {
  Card,
  CardBody,
  CardTitle,
  Col,
  Row,
} from 'reactstrap';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { getStyle } from '@coreui/coreui/dist/js/coreui-utilities'
import { GraphQLClient } from 'graphql-request'


const client = new GraphQLClient('https://api.graph.cool/simple/v1/cjrzop3ic07bj0189m1a7yw5l', {
  headers: {
    Authorization: 'Bearer YOUR_AUTH_TOKEN',
  },
});

const evaluation_query = () => {
  return client.request(`
    {
      allPerformances(
        orderBy:created_DESC
        filter:{
          created_gt:"2019-04-04T03:38:41.987Z"
          created_lt:"2019-04-24T03:38:41.987Z"
        }
      ){
        created
        success
        company
        health
        effort
        virtues
        emotions
        risks
        obstacles
        opportuinities
      }
    }
  `)
}

const text_to_int = (text) =>{
  switch(text) {
    case 'One': return 1
    case 'Two': return 2
    case 'Three': return 3
    default: return null
  }
}

const Widget03 = lazy(() => import('../../views/Widgets/Widget03'));

const brandPrimary = getStyle('--primary')
const brandSuccess = getStyle('--success')
const brandInfo = getStyle('--info')
const brandDanger = getStyle('--danger')

const card_labels = ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']
const health_data = [2, 2, 3, 2, 3, 2, 3, 3, 3, 1, 2, 3, 3, 3, 2]
const success_data = [2, 3, 3, 2, 3, 2, 3, 3, 3, 2, 2, 3, 3, 3, 3]
const company_data = [3, 3, 2, 3, 2, 2, 2, 1, 2, 3, 3, 2, 3, 2, 2]
const effort_data = [2, 2, 2, 2, 1, 2, 1, 1, 1, 2, 2, 2, 2, 2, 3]

const cardHealthData = {
  labels: card_labels,
  datasets: [
    {
      label: 'Índice de Éxito',
      backgroundColor: brandPrimary,
      borderColor: 'rgba(255,255,255,.55)',
      data: company_data,
    },
  ],
};

const cardChartOpts1 = {
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data) {
        const label = data.datasets[0].data[tooltipItem.index]
        if (label === 3) {
          return 'Compañia     1'
        }
        else if (label === 1) {
          return 'Compañia     3'
        }
        else{
          return 'Compañia     2'
        }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
        },
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
}


const cardSuccessData = {
  labels: card_labels,
  datasets: [
    {
      backgroundColor: brandInfo,
      borderColor: 'rgba(255,255,255,.55)',
      data: success_data,
    },
  ],
};

const cardChartOpts2 = {
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data) {
        const label = data.datasets[0].data[tooltipItem.index]
        if (label === 3) {
          return 'Éxito     1'
        }
        else if (label === 1) {
          return 'Éxito     3'
        }
        else{
          return 'Éxito     2'
        }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          color: 'transparent',
          zeroLineColor: 'transparent',
        },
        ticks: {
          fontSize: 2,
          fontColor: 'transparent',
        },

      }],
    yAxes: [
      {
        display: false,
        ticks: {
          display: false,
          min:1,
          max:3
        },
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

const cardCompanyData = {
  labels: card_labels,
  datasets: [
    {
      label: 'Índice de Salud',
      backgroundColor: 'rgba(255,255,255,0)',
      borderColor: 'rgba(255,255,255,.55)',
      data: health_data,
    },
  ],
};

const cardChartOpts3 = {
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data) {
        const label = data.datasets[0].data[tooltipItem.index]
        if (label === 3) {
          return 'Salud     1'
        }
        else if (label === 1) {
          return 'Salud     3'
        }
        else{
          return 'Salud     2'
        }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }
    ]
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

const cardEffortData = {
  labels: card_labels,
  datasets: [
    {
      label: 'Índice de Esfuerzo',
      backgroundColor: 'rgba(255,255,255,0)',
      borderColor: 'rgba(255,255,255,.55)',
      data: effort_data,
    },
  ],
};

const cardChartOpts4 = {
  tooltips: {
    callbacks: {
      label: function(tooltipItem, data) {
        const label = data.datasets[0].data[tooltipItem.index]
        if (label === 3) {
          return 'Esfuerzo     1'
        }
        else if (label === 1) {
          return 'Esfuerzo     3'
        }
        else{
          return 'Esfuerzo     2'
        }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
        barPercentage: 0.6,
      }],
    yAxes: [
      {
        display: false,
        min: 0,
        max: 4,
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

const socialBoxData = [
  { data: [65, 59, 84, 84, 51, 55, 40], label: 'facebook' },
  { data: [0, 1, 4, 5, 8, 6, 0], label: '' },
  { data: [78, 81, 80, 45, 34, 12, 40], label: 'linkedin' },
  { data: [35, 23, 56, 22, 97, 23, 64], label: 'google' },
];

const makeSocialBoxData = (dataSetNo) => {
  const dataset = socialBoxData[dataSetNo];
  const data = {
    labels: ['', '',  '', '', '', ''],
    datasets: [
      {
        backgroundColor: 'rgba(255,255,255,.1)',
        borderColor: 'rgba(255,255,255,.55)',
        pointHoverBackgroundColor: '#fff',
        borderWidth: 2,
        data: dataset.data,
        label: dataset.label,
      },
    ],
  };
  return () => data;
};

const socialChartOpts = {
  tooltips: {
    enabled: false,
  },
  responsive: true,
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        display: false,
      }],
    yAxes: [
      {
        display: false,
      }],
  },
  elements: {
    line: {
      borderWidth: 2,
    },
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
    },
  },
};

const mainChart = (labels, data) => ({
  labels: labels,
  datasets: [
    {
      label: 'Número de Ocurrencias:',
      backgroundColor: 'transparent',
      borderColor: brandSuccess,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data,
    }
  ]
})

const secondaryChart = (labels, data) => ({
  labels: labels,
  datasets: [
    {
      label: 'Número de Ocurrencias:',
      backgroundColor: 'transparent',
      borderColor: brandDanger,
      pointHoverBackgroundColor: '#fff',
      borderWidth: 2,
      data: data,
    }
  ]
})

const mainChartOpts = {
  tooltips: {
    enabled: false,
    custom: CustomTooltips,
    intersect: true,
    mode: 'index',
    position: 'nearest',
    callbacks: {
      labelColor: function(tooltipItem, chart) {
        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor }
      }
    }
  },
  maintainAspectRatio: false,
  legend: {
    display: false,
  },
  scales: {
    xAxes: [
      {
        gridLines: {
          drawOnChartArea: false,
        },
      }],
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 5,
          stepSize: Math.ceil(250 / 5),
          max: 10,
        },
      }],
  },
  elements: {
    point: {
      radius: 0,
      hitRadius: 10,
      hoverRadius: 4,
      hoverBorderWidth: 3,
    },
  },
};

class Dashboard extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.onRadioBtnClick = this.onRadioBtnClick.bind(this);

    this.state = {
      dropdownOpen: false,
      radioSelected: 2,
      health: [],
      success: [],
      company: [],
      effort: [],
      virtues: {},
      emotions: {},
      risks: [],
      obstacles: [],
      opportuinities: []
    };
  }

  async componentDidMount(){
    const { allPerformances } = await evaluation_query()
    const health = allPerformances.map(performance => text_to_int(performance.health))
    const success = allPerformances.map(performance => text_to_int(performance.success))
    const company = allPerformances.map(performance => text_to_int(performance.company))
    allPerformances.map(performance => console.log(performance.virtues, performance.emotions))
    const effort = allPerformances.map(performance => text_to_int(performance.effort))
    const risks = allPerformances.map(performance => performance.risks).filter(risks => risks)
    const obstacles = allPerformances.map(performance => performance.obstacles).filter(obstacles => obstacles)
    const opportuinities = 
      allPerformances
      .map(performance => performance.opportuinities)
      .filter(opportuinities => opportuinities)

    const virtues = {}
    allPerformances.forEach(performance=>
      performance.virtues
      ? performance.virtues.forEach(virtue=>
          virtues[virtue] ? virtues[virtue] +=1 : virtues[virtue] = 1
        )
      : null
    )

    const array_of_virtues = Object.keys(virtues).map(virtue => ({[virtue]: virtues[virtue]}))
    const sorted_virtues = array_of_virtues.sort((a,b) => 
      Object.keys(a)[0] > Object.keys(b)[0] ? 1 : -1
    )

    const emotions = {}
    allPerformances.forEach(performance=>
      performance.emotions
      ? performance.emotions.forEach(emotion=>
          emotions[emotion] ? emotions[emotion] +=1 : emotions[emotion] = 1
        )
      : null
    )

    const array_of_emotions = Object.keys(emotions).map(emotion => ({[emotion]: emotions[emotion]}))
    const sorted_emotions = array_of_emotions.sort((a,b) => 
      Object.keys(a)[0] > Object.keys(b)[0] ? 1 : -1
    )

    this.setState({
      health: health, 
      success: success, 
      company: company, 
      effort: effort, 
      virtues:sorted_virtues,
      emotions: sorted_emotions,
      risks: risks,
      obstacles: obstacles,
      opportuinities: opportuinities
    })

    console.log(
      'health:', health, 
      'success:', success, 
      'company:', company, 
      'effort:', effort, 
      'virtues:', sorted_virtues,
      'emotions:', sorted_emotions,
      'risks:', risks,
      'obstacles:', obstacles,
      'opportuinities:', opportuinities
    )
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen,
    });
  }

  onRadioBtnClick(radioSelected) {
    this.setState({
      radioSelected: radioSelected,
    });
  }

  loading = () => <div className="animated fadeIn pt-1 text-center">Loading...</div>

  render() {

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-info">
              <CardBody className="pb-0">
                <div className="text-value">
                  { this.state.success.length > 0
                    ? Math.round(this.state.success.reduce( ( p, c ) => p + c, 0 ) / this.state.success.length  * 100)/100
                    : 0
                  }
                </div>
                <div>Índice de Éxito</div>
              </CardBody>
              <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                <Line data={cardSuccessData} options={cardChartOpts2} height={70} />
              </div>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-primary">
              <CardBody className="pb-0">
                <div className="text-value">
                  {
                    this.state.company.length > 0
                    ? Math.round(this.state.company.reduce( ( p, c ) => p + c, 0 ) / this.state.company.length * 100)/100 
                    : 0
                  }
                </div>
                <div>Índice de Compañia</div>
              </CardBody>
              <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                <Line data={cardHealthData} options={cardChartOpts1} height={70} />
              </div>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-warning">
              <CardBody className="pb-0">
                <div className="text-value">
                  {  
                    this.state.health.length > 0
                    ? Math.round(this.state.health.reduce( ( p, c ) => p + c, 0 ) / this.state.health.length * 100)/100
                    : 0
                  }
                </div>
                <div>Índice de Salud</div>
              </CardBody>
              <div className="chart-wrapper" style={{ height: '70px' }}>
                <Line data={cardCompanyData} options={cardChartOpts3} height={70} />
              </div>
            </Card>
          </Col>

          <Col xs="12" sm="6" lg="3">
            <Card className="text-white bg-danger">
              <CardBody className="pb-0">
                <div className="text-value">
                  {
                    this.state.effort.length > 0
                    ? Math.round(this.state.effort.reduce( ( p, c ) => p + c, 0 ) / this.state.effort.length * 100)/100
                    : 0
                  }
                </div>
                <div>Índice de Esfuerzo</div>
              </CardBody>
              <div className="chart-wrapper mx-3" style={{ height: '70px' }}>
                <Line data={cardEffortData} options={cardChartOpts4} height={70} />
              </div>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Virtudes</CardTitle>
                    <div className="small text-muted">Abril 2019</div>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                  </Col>
                </Row>
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                  <Bar 
                    data={
                      mainChart(
                        this.state.virtues.length > 0 ? this.state.virtues.map(virtue=>Object.keys(virtue)[0]) : [],
                        this.state.virtues.length > 0 ? this.state.virtues.map(virtue=>virtue[Object.keys(virtue)[0]]) : []
                      )
                    } 
                    options = { mainChartOpts } 
                    height = { 300 } 
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>
        
        <Row>
          <Col>
            <Card>
              <CardBody>
                <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Emociones</CardTitle>
                    <div className="small text-muted">Abril 2019</div>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                  </Col>
                </Row>
                <div className="chart-wrapper" style={{ height: 300 + 'px', marginTop: 40 + 'px' }}>
                  <Bar 
                    data={
                      secondaryChart(
                        this.state.emotions.length > 0 ? this.state.emotions.map(emotion=> Object.keys(emotion)[0]) : [],
                        this.state.emotions.length > 0 ? this.state.emotions.map(emotion=> emotion[Object.keys(emotion)[0]]) : []
                      )
                    } 
                    options = { mainChartOpts } 
                    height = { 300 } 
                  />
                </div>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col>
            <Card>
              <CardBody>
               <Row>
                  <Col sm="5">
                    <CardTitle className="mb-0">Oprtunidades, Obstáculos y Riesgos</CardTitle>
                    <div className="small text-muted">Análisis por Frecuencia</div>
                  </Col>
                  <Col sm="7" className="d-none d-sm-inline-block">
                  </Col>
                </Row>
                <Row>
                  <Col>
                    <img
                      style = {{ marginLeft: '15%', width:'70%', marginTop:25 }} 
                      src="../../../assets/img/cloud-word.png" alt="word cloud"/>
                  </Col>
                </Row>
              </CardBody>
            </Card>
          </Col>
        </Row>

        <Row>
          <Col xs="12" sm="12" lg="12">
            <Suspense fallback={this.loading()}>
              <Widget03 dataBox={() => ({ variant: 'twitter', SI: '4', NO: '1' })} >
                <div className="chart-wrapper">
                  <Line data={makeSocialBoxData(1)} options={socialChartOpts} height={90} />
                </div>
              </Widget03>
            </Suspense>
          </Col>
        </Row>

      </div>
    );
  }
}

export default Dashboard;
